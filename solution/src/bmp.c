#include "bmp.h"
#include <stdlib.h>
#define BMP_HEADER_SIZE 40
#define BITS_PER_PIXEL 24
#define BYTES_PER_PIXEL 3
#define BMP_FILE_TYPE 0x4D42
#define BMP_PADDING 4


struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

int from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * sizeof(struct pixel));

    if (!img->data) {
        return READ_ERROR;
    }

    for (uint32_t row = 0; row < header.biHeight; ++row) {
        
        int img_result = (int)fread(img->data + row * img->width, sizeof(struct pixel), header.biWidth, in);
        if (img_result != img->width) {
            free(img->data);
            return READ_ERROR;
        }
        
        if(fseek(in, (int)(4 - (img->width * sizeof(struct pixel)) % 4) % 4, SEEK_CUR)!=0){
            free(img->data);
            return READ_ERROR;
        }
    }
    
    return READ_SUCSESS;
}



int to_bmp(FILE* file, struct image* img) {
    struct bmp_header header;
    header.bfType = BMP_FILE_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = BITS_PER_PIXEL;
    header.biCompression = 0;
    uint8_t padding = (4 - (header.biWidth * sizeof(struct pixel)) % 4) % 4;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;

    header.biClrImportant = 0;

    int header_result = (int)fwrite(&header, sizeof(struct bmp_header), 1, file);
    
    if (header_result!= 1) {
        return WRITE_ERROR;
    }
    
    uint32_t i = 0;
    while (i < img->height) {
        fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, file); 
        if (padding > 0) {
                uint8_t paddingData[3] = {0};
                int padding_result = (int)fwrite(paddingData, 1, padding, file);
                
                if  (padding_result != padding) {
                return WRITE_ERROR;
            }
        }
        i+=1;
    }
    return WRITE_SUCSESS;
}
