#include <stdint.h>
#include <stdio.h>

#ifndef IMAGE_H
#define IMAGE_H


struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
}__attribute__((packed));

struct image {
    uint64_t width;
    uint64_t height;
    int* pixel;
    struct pixel* data;
};

#endif

