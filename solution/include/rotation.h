#include "image.h"
#include <stdint.h>


#ifndef ROTATION_H
#define ROTATION_H
#define ANGLE_STEP 90
#define MAX_ANGLE 270
#define MIN_ANGLE (-270)
#define LINE_ANGLE 180


struct image rotate_image(struct image* source, int angle);
void free_image(struct image* img);

#endif

