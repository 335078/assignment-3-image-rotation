#include "image.h"
#include <stdio.h>

#ifndef BMP_H
#define BMP_H

#define READ_ERROR 1
#define READ_SUCSESS 0
#define WRITE_ERROR 1
#define WRITE_SUCSESS 0


int from_bmp(FILE* in, struct image* img);
int to_bmp(FILE* file, struct image* img);

#endif

